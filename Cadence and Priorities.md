# Weekly
## Monday: 
Typically Mondays are dedicated to async work, so they tent to be 1-2-1s free
1. Ent SA Team StandUp 
2. Activities report
These first two are important to understand SAs workload and in which areas we are struggling as a team
3. EMEA Top Deal Reviews to identify blockers, problems and gaps

## Tuersday 
SA Leadership Team Management (every second Tuesday for EMEA friendly time)

## Wednesday 
Marketing activities Proposals Review

## Thursday 
SEUR Forecast call with Sales, CSM, PS, Channel

## Friday
Learning & Development via Shadowing, Book reading and on demand courses

## During the Weekly
- 1-2-1s
- coffee chats (Limit to 3)
- Mornings usually for Focus Time and Async work
- Attending recurrent meeting for team building as well as learn and take actions to support the business and the individuals no matter the department/division
- From Tuersday till thursday, I review Notes for 1-2-1s prior to the calls and follow up on actions taken from these calls. I join customers calls to facilitate deals moving forward and provide SAs with help and with feedback they can work on

# Monthly
-

# Quarterly
- [Individual Career Plans](https://lucid.app/lucidspark/88a88a73-c13d-443a-a73f-fd8e3660cb0b/edit?page=0_0&invitationId=inv_0e821fcd-7c91-487a-8ba3-75e2b3464f38#)

# Annually
- Talent Assessment and Promotions
