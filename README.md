# Alessia's README (Manager, Solutions Architects, Enterprise SEUR)

Thanks for stopping by. If you want to know more about what I am passionate about, keep reading ;-).

## A Little about myself

I was born and raised in Italy and I consider this a huge luck. I have studied Telecommunications and Computer Science Engineering in Perugia and then moved to London to complete my thesis work. 

I moved to London in 2010 and I lived there for 12 years. I have worked first in a Satellite Operator (Inmarsat now VIASAT) as a RAN Engineer and User Termninal Engineer. Then moved more into a software architecture role until my curiosity led me to microservices architecture and the cloud. 
That's when I got my first sales engineering role in Pivotal, a life changing workplace. I have lived the Pivotal dream for less than 2 years, until VMware acquired Pivotal. 
In VMware my journey continued as Sales Engineer until I got the chance to become a people manager and here I am at GitLab. 

I have relocated back in Italy in 2021 for family reasons, but I loved London a lot and who knows?!?!

I love travelling, outdoor sports, art exhibitions and a good italian aperitivo surrounded by good people! It has been a while since I went to a show, but I love that too!

## [How I operate](https://gitlab.com/adelvecchio/adelvecchio/-/blob/main/Cadence%20and%20Priorities.md)

I have three simple pilars: 
1. Run the Business
2. Run the Team and GitLab Customers
3. Run yourself

I set GCalendar tasks that remind me what recurrent tasks I have to complete during the week/month/year.

I tent to have mornings for Focus Time to progress with activities like SAs Career Dev Plan/Reviews, removing roadblockers for the sales team, collaborate with peers on operationalising the way we work across the company better, hiring and some personal Learning & Development. 
Coffee chats are always welcome: I tent to have no more than 3 a week.

## Things I value

- Kindness: always always always be kind. You do not know what battle the people in front of you are going through. 
- Trust: I trust you will do the right thing for you, the people around you and GitLab. I trust you are also a good manager of one and will reach out for help when needed. 
- Humbleness: Everyone is doing the best they can given the circumstances they are in. There is so much to learn out there and space for everyone to be a leader or to be a follower.
- Honesty: if you speak your heart, no matter how hard what you say is to take in, I respect you and will work with you to do better.
- Enpowerment: I like challenges and to be able to sort things out by myself gives me prode. So I operate under the assumption that if you need help, you will always ask for it. Said that, if I can read from body language that someone is having a hard time to speak their voice, I might offer to be your voice to help you

## My career ambitions
I found that one of the most difficult things in Sales Engineering roles (and in life in general) is influencing decisions internally at GitLab or at our customers, hence why influencing is one of my top career goal. It is a lot about working on yourself, your self control, your non judgemental attitude, your patience but also meeting people where they are and truly understand how they see things and make people feel heard. I think the Solutions Architecture Division allows this growth in thinking more strategically to making a bigger impact on our customers and so on GitLab. 
